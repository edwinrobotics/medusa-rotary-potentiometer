# Medusa Rotary Potentiometer

![Medusa Rotary Potentiometer](https://shop.edwinrobotics.com/4141-thickbox_default/medusa-rotary-potentiometer.jpg "Medusa Rotary Potentiometer")

[Medusa Rotary Potentiometer](https://shop.edwinrobotics.com/modules/1234-medusa-rotary-potentiometer.html)

The Medusa Rotary Potentiometer uses a center tap linear type plug in potentiometer, ie the resistance will linearly vary with its position. 


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.